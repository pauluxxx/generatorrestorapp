import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // <--- JavaScript import from Angular
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {AppComponent} from './app.component';
import {HttpModule} from '@angular/http';
import {TopMenuComponent} from './common/top-menu/top-menu.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './common/home/home.component';
import {GeneratorSevice} from './common/shared/services/generator-sevice';
import {GeneratorComponent} from './components/generator/generator.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatInputModule, MatSliderModule, MatButtonModule, MatExpansionModule} from '@angular/material';
import {OptionComponet} from './components/option/option.componet';
import {OptionService} from './common/shared/services/option-service';
import {ParametersComponent} from './components/parameters/parameters.component';
import {AutoWidthDirective} from './directives/auto-width.directive';
import {ExperimentService} from './common/shared/services/experement-service';
import {HttpClientModule} from '@angular/common/http';
import {AboutComponent} from "./common/about/about.component";
import {TeamComponent} from "./common/team/team.component";
import {ChartsModule} from "ng2-charts";
import {ChartComponent} from "./common/chart/chart.component";
import {MatDialogModule} from '@angular/material';
import {RestoreModalComponent} from "./common/chart/restore-modal/restore.modal.component";

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    HomeComponent,
    AboutComponent,
    TeamComponent,
    GeneratorComponent,
    OptionComponet,
    ParametersComponent,
    AutoWidthDirective,
    ChartComponent,
    RestoreModalComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSliderModule,
    MatButtonModule,
    MatInputModule,
    MatExpansionModule,
    ChartsModule,
    MatDialogModule
  ],
  providers: [GeneratorSevice, OptionService, ExperimentService],
  bootstrap: [AppComponent],
  entryComponents: [RestoreModalComponent]
})
export class AppModule {
}
