import {Parameter} from './parameter';

export class Option {
  constructor(public id: number,
              public name: string,
              public paramters: Parameter[]) {
  }
}
