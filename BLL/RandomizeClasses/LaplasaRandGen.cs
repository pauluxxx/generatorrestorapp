﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using Troschuetz.Random.Distributions.Continuous;
using System.Linq;

namespace BLL.RandomizeClasses
{
    class LaplasaRandGen : IRandGenerator
    {
        public LaplaceDistribution laplasa { get; set; }
        public double m { get; private set; }
        public double b { get; private set; }

        public LaplasaRandGen()
        {
            m = 0;
            b = 1;
            laplasa = new LaplaceDistribution();
            laplasa.Mu = m;
            laplasa.Alpha = b;
        }
        public double nextDouble()
        {
            return laplasa.NextDouble();
        }

        public void TuneGenertor(Generator gen)
        {
            b = (float)gen.GeneratorParameters.FirstOrDefault(e => e.Parameter.Key == "b").Parameter.Value;
            m = (float)gen.GeneratorParameters.FirstOrDefault(e => e.Parameter.Key == "m").Parameter.Value;
            laplasa.Mu = m;
            laplasa.Alpha = b;
        }

        public double getMean()
        {
            return laplasa.Mean;
        }

        public double getVariance()
        {
            return laplasa.Variance;
        }
    }
}
