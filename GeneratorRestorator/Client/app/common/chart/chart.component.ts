import {Component, OnInit, ViewChild} from '@angular/core';
import {ExperimentService} from '../shared/services/experement-service';
import {MatDialog} from '@angular/material';
import {RestoreModalComponent} from './restore-modal/restore.modal.component';
import {CHART_CONFIG} from './ChartConfiguration';
import {BaseChartDirective} from 'ng2-charts';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-chart',
  templateUrl: 'chart.component.html',
  // styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  public lineChartData: Array<any> = [{data: {}, label: ''}];
  public varience: Array<number> = [];
  public mean: Array<number> = [];
  public min: Array<number> = [];
  public max: Array<number> = [];
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  protected get hasData() {
    return !!this.experimentService.experimentResults;
  }

  constructor(private experimentService: ExperimentService,
              private dialog: MatDialog, private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  public openRestoreDialog() {
    //spoki Pavel
    const dialogRef = this.dialog.open(RestoreModalComponent, {
      height: '400px',
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(({data, method}) => {
      console.log(data);
      this.http.post(`api/Restoring/${method}`, {data: data},
        this.httpOptions).subscribe((res) => {
        this.addDataOnGraph(res);
        this.chart.ngOnChanges({});
      });
    });
  }

  public resetGraph() {
    this.experimentService.resert();
    this.lineChartData = [{data: {}, label: ''}];
    this.chart.ngOnChanges({});

  }

  public ngOnInit() {
    if (!this.experimentService.experimentResults) {
      console.log('No data!');
      return;
    }
    const res = this.experimentService.experimentResults;
    res.map((data) => {
      this.addDataOnGraph(data);
    });
  }

  private addDataOnGraph(data: any) {
    const x = Object.keys(data.data);
    console.log(x);
    const lastX = x.pop();
    this.lineChartData.push({
      data: Object.entries(data.data)
        .map(([x, y]) => ({x, y}))
        .filter(({x, y}) => y > 0)
        .concat({x: lastX, y: 0}),
      label: data.name
    });
    this.max.push(data.max);
    this.min.push(data.min);
    this.varience.push(data.varience);
    this.mean.push(data.mean);
  }


  public lineChartOptions = CHART_CONFIG.CHART_OPTIONS;
  public lineChartColors = CHART_CONFIG.COLORS;
  public lineChartLegend: boolean = CHART_CONFIG.LEGEND;
  public lineChartType: string = CHART_CONFIG.TYPE;


}
