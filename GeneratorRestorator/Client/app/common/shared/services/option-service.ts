import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Option} from '../models/option';
import {Parameter} from '../models/parameter';
import {OPTIONS} from '../../mocks/mock-options';
import 'rxjs/add/operator/map';

@Injectable()
export class OptionService {
  private generatorsUrl = 'api/option/';

  constructor(private http: Http) {
  }

  getOptions(optName: string): Promise<Option> {
    return this.http.get(this.generatorsUrl + optName).toPromise().then(
      response => {
        let option: Option = new Option(0, null, null);
        var params: Parameter[] = [];
        response.json().optionParameter.map(function (el) {
          params.push({
            key: el.parameter.key,
            value: el.parameter.value, min: el.parameter.min, max: el.parameter.max, step: el.parameter.step
          });
        });
        option.id = response.json().id;
        option.name = response.json().name;
        option.paramters = params;
        return option;
      }
    ).catch(this.handleError);
  }

  getOptionsMock(): Promise<any []> {
    return Promise.resolve(OPTIONS);
  }

  private handleError(err: any): Promise<any> {
    console.error('Error with retrieving data from generators api', err);
    return Promise.reject(err.message || err);
  }
}
