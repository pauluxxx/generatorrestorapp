﻿using DAL.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BLL.Managers.Abstact
{
    public interface IOptionManager
    {
        Option getOption(string name);
        Option getOption(int id);
        IEnumerable<Option> getOptions();
    }
}
