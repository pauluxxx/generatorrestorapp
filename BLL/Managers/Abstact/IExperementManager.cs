﻿using DAL.Entities;
using GeneratorRestorator.Server.ClientEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Managers.Abstact
{
    public interface IExperementManager
    {
        ResultsGenerating startExperement(Experement v);
        IDictionary<double, double> getRestoredInterpolation(DataClass data);
        IDictionary<double, double> getRestoredMNK(DataClass data);
    }
}
