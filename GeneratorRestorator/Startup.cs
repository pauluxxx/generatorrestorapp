﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;
using BLL.Managers.Abstact;
using BLL.Managers;
using Microsoft.Extensions.Configuration;
using DAL;
using DAL.Repositories;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using DAL.DBSeeds;
using BLL.Core;
using BLL.Core.IO;
using BLL.Core.IO.Implementation;

namespace GeneratorRestorator
{
  public class Startup
  {
    public IConfigurationRoot Configuration { get; }
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();
      Configuration = builder.Build();
    }
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      string connectionString = Configuration.GetConnectionString("DidencoConnection");
      //db context working fine 
      services.AddScoped<IDidenkoDb, DidenkoDB>();
      services.AddDbContext<DidenkoDB>(options => options.UseMySQL(connectionString));
      //repos
      services.AddTransient<IRepository<Generator>, GeneratorRepository>();
      services.AddTransient<IRepository<Option>, OptionRepository>();
      services.AddTransient<IRepository<Experement>, ExperementRepository>();
      //managers
      services.AddTransient<IGeneratorManager, GeneratorManager>();
      services.AddTransient<IOptionManager, OptionManager>();
      services.AddTransient<IExperementManager, ExperementManager>();
      //core
      services.AddTransient<IExperementExequtor, ExperementExequtor>();
      //io
      services.AddSingleton<IRAMLocalStorage, RAMLocalStorage>();

      services.AddMvc().AddJsonOptions(
            options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        ); ;
      services.AddCors();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      //Reditect any non-Api calls to the Angular app
      // so our app can handle the routing
      app.Use(async (context, next) =>
      {
        await next();
        if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) && !context.Request.Path.Value.StartsWith("/api/"))
        {
          context.Request.Path = "index.html";
          await next();
        }
      });
      //Configures app for usage as API
      //with default controller '/api/[Controller]'
      app.UseMvcWithDefaultRoute();

      //config app to serve index.html file from /wwwroot
      //when you access the server from a wev browser
      app.UseDefaultFiles();
      app.UseStaticFiles();
    }
  }
}
