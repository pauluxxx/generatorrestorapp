﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Core.IO.Implementation
{
    public class RAMLocalStorage : IRAMLocalStorage
    {
        private IDictionary<int, IDictionary<double, double>> LocalExperemts { get; set; }
        public RAMLocalStorage()
        {
            LocalExperemts = new Dictionary<int, IDictionary<double, double>>();
        }

        public IDictionary<double, double> getExperementResults(int id)
        {
            return LocalExperemts[id];
        }

        public void LoadResultToStorage(int Expid, IDictionary<double, double> responceNumbers)
        {
            LocalExperemts.Add(Expid, responceNumbers);
        }
    }
}
