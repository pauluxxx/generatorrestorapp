﻿using System.Collections.Generic;
using System.Linq;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ExperementRepository : IRepository<Experement>
    {
        private IDidenkoDb _db;
        public ExperementRepository(IDidenkoDb _db)
        {
            this._db = _db;
        }
        public IDidenkoDb _dbContext
        {
            get { return this._db; }
        }

        public IEnumerable<Experement> Items
        {
            get { return _dbContext.Experements.Include(g => g.Option).Include(g => g.Generator); }
        }

        public Experement Get(int id)
        {
            return
                Items.FirstOrDefault(e => e.Id == id);
        }

        public void Create(Experement item)
        {
            _dbContext.Experements.Add(item);
            _dbContext.saveChanges();
        }


        public void Update(Experement item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Experement item)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Experement> GetAll()
        {
            throw new System.NotImplementedException();
        }
    }
}