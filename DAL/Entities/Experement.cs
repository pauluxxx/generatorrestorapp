﻿using GeneratorRestorator.Server.ClientEntities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class Experement :IEntity
    {
        public int Id { get; set; }

        [Required]
        public Generator Generator { get; set; }

        [Required]
        public Option Option { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string TimeOfGeneration { get; set; }
        public Experement() { }
        public Experement(ClientExperement ex) {
            this.Generator = new Generator(ex.Generator);
            this.Option = new Option(ex.Option);
        }
    }
}