﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class Parameter :IEntity
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public float Value { get; set; }
        public float Min { get; set; }
        public float Max { get; set; }
        public float Step { get; set; }

        public IEnumerable<OptionParameter> OptionParameter { get; set; }
        public IEnumerable<GeneratorParameter> GeneratorParameters { get; set; }


    }
}