﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.RandomizeClasses
{
    public interface IRandGenerator
    {
        double nextDouble();
        void TuneGenertor(Generator gen);
        double getMean();
        double getVariance();
    }
}
