export class Parameter {
  key: string;
  value: number;
  min: number;
  max: number;
  step: number;
}
