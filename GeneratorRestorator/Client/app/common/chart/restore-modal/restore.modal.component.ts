import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormControl, FormGroup} from '@angular/forms';
import {ExperimentService} from '../../shared/services/experement-service';

@Component({
  selector: 'app-restore-modal',
  templateUrl: 'restore.modal.component.html',
  // styleUrls: ['./chart.component.scss']
})
export class RestoreModalComponent implements OnInit {

  public form: FormGroup = new FormGroup({
    s1: new FormControl(),
    s2: new FormControl()
  });

  public ngOnInit(): void {
    this.graphData = this.experimentService.experimentResults;
    this.methods = ['MNK', 'Interpolation'];
  }

  public graphData: Array<any> = [];
  public methods: Array<any> = [];

  constructor(public dialogRef: MatDialogRef<RestoreModalComponent>,
              private experimentService: ExperimentService) {
  }

  public close() {

    this.dialogRef.close(this.form.value);
    const data = this.form.get('s1').value && this.form.get('s1').value.data;
    const method = this.form.get('s2').value;
    const x = Object.keys(data);

    this.dialogRef.close({data, method});
  }
}
