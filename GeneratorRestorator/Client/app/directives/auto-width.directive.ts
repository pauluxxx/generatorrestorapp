import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[app-auto-grow]',
})
export class AutoWidthDirective {
  @Input() max_input_numbers: number = 40;

  @HostListener('input', ['$event.target'])
  onInput(textArea: HTMLTextAreaElement): void {
    if (this.element.nativeElement.value.length <= this.max_input_numbers) {
      this.adjust();
    } else {
      console.log(this.element.nativeElement.style.width);
    }
  }
  ngAfterContentChecked(): void {
    this.adjust();
  }
  constructor(public element: ElementRef) {
  }

  adjust(): void {
    let nativeElement = this.element.nativeElement;
    this.resizable(nativeElement, 13);
  }

  private resizable(el, factor) {
    let int = Number(factor) || 7.7;

    function resize() {
      el.style.width = ((el.value.length + 1) * int) + 'px';
    }

    resize();
  }
}
