﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL.Managers.Abstact;
using System.Collections;
using DAL.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GeneratorRestorator.Server.Controllers
{
  [Route("api/[controller]")]
  public class OptionController : Controller
  {
    private IOptionManager manager;
    public OptionController(IOptionManager manager)
    {
      this.manager = manager;
    }
    // GET: api/values
    [HttpGet]
    public IEnumerable Get()
    {
      return manager.getOptions();
    }

    // GET api/values/5
    [HttpGet("{name}")]
    public Option Get(string name)
    {
      return manager.getOption(name);
    }
   
    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
