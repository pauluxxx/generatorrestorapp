﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL.Managers.Abstact;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DAL.Entities;
using System.Collections;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GeneratorRestorator.Server.Controllers
{
  [Route("api/[controller]")]
  public class GeneratorController : Controller
  {
   private IGeneratorManager manager;
    public GeneratorController(IGeneratorManager manager)
    {
      this.manager = manager;
    }
    [HttpGet]
    public IEnumerable Get()
    {
      var s = manager.GeneratorsWithParameters();
      return s;
    }

    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
