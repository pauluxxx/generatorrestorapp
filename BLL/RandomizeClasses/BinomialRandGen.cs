﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Troschuetz.Random.Distributions.Discrete;

namespace BLL.RandomizeClasses
{
    public class BinomialRandGen : IRandGenerator
    {
        //in this class must be manifest names of values about parameters
        public double n { get; set; }
        public double p { get; set; }
        public BinomialDistribution binom { get; set; }
        public BinomialRandGen()
        {
            binom = new BinomialDistribution();
            n = 10;
            p = .2;
            binom.Alpha = p;
            binom.Beta = (int)n;
        }

        public double nextDouble()
        {
            return binom.NextDouble();
        }
        public void TuneGenertor(Generator gen)
        {
            n = (float)gen.GeneratorParameters.FirstOrDefault(e => e.Parameter.Key == "n").Parameter.Value;
            p = (float)gen.GeneratorParameters.FirstOrDefault(e => e.Parameter.Key == "p").Parameter.Value;
            binom.Alpha = p;
            binom.Beta = (int)n;
        }

        public double getMean()
        {
            return binom.Mean;
        }

        public double getVariance()
        {
            return binom.Variance;
        }
    }
}
