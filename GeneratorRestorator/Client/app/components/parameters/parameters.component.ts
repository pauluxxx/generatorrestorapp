import {Parameter} from '../../common/shared/models/parameter';
import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-parameters',
  templateUrl: 'parameter.component.html'
})
export class ParametersComponent {
  @Input() parameters: Parameter[];
}
