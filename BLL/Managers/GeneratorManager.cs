﻿using BLL.Managers.Abstact;
using DAL;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using DAL.Repositories;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;

namespace BLL.Managers
{
    public class GeneratorManager : IGeneratorManager
    {
        IRepository<Generator> repository;
        public GeneratorManager(IRepository<Generator> repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Generator> Generators()
        {
            return repository.GetAll();
        }

        public IEnumerable GeneratorsWithParameters()
        {
            

            return repository.Items.Select(e => new { Id = e.Id, Title = e.Title, Parameters = e.GeneratorParameters.Select(el => new Parameter(){ Id = el.Parameter.Id, Key = el.Parameter.Key, Max = el.Parameter.Max, Min = el.Parameter.Min, Value = el.Parameter.Value, Step = el.Parameter.Step }) });

        }
        public Generator getGenerator(int id)
        {
            return repository.Get(id);
        }
    }
}
