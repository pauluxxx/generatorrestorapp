﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GeneratorRestorator.Server.ClientEntities;
using BLL.Managers.Abstact;

namespace GeneratorRestorator.Server.Controllers
{

  [Route("api/[controller]")]
  public class RestoringController : Controller
  {

    private IExperementManager manager;
    public RestoringController(IExperementManager manager)
    {
      this.manager = manager;
    }
    // GET: api/Restoring
    [HttpGet]
    public IEnumerable<string> Get()
    {
      return new string[] { "value1", "value2" };
    }

    [HttpGet("{name}")]
    public ResultsGenerating Post(string name)
    {
      return null;
    }

    // POST: api/Restoring
    [HttpPost("[action]")]
    public ResultsGenerating MNK([FromBody] DataClass data)
    {
      return new ResultsGenerating { Name = "RESTORED",Data=manager.getRestoredMNK(data) };
    } 
    [HttpPost("[action]")]
    public ResultsGenerating Interpolation([FromBody] DataClass data)
    {
      return new ResultsGenerating { Name = "da" , Data = manager.getRestoredInterpolation(data) };
    }
    // PUT: api/Restoring/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE: api/ApiWithActions/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }


  }
}
