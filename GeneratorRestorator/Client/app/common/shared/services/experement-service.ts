///<reference path="../../../../../node_modules/rxjs/Observable.d.ts"/>

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Experement} from '../models/experement';
import {Observable} from 'rxjs/Observable';
import {catchError, map, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

@Injectable()
export class ExperimentService {
//new feature  public experimentResult: any = {"0":0.0,"0.1":0.0,"0.2":0.0,"0.3":0.0,"0.4":0.0,"0.5":0.0,"0.6":0.0,"0.7":0.0,"0.8":0.0,"0.9":0.0,"1":0.00982,"1.1":0.0,"1.2":0.0,"1.3":0.0,"1.4":0.0,"1.5":0.0,"1.6":0.0,"1.7":0.0,"1.8":0.0,"1.9":0.04398,"2":0.0,"2.1":0.0,"2.2":0.0,"2.3":0.0,"2.4":0.0,"2.5":0.0,"2.6":0.0,"2.7":0.0,"2.8":0.0,"2.9":0.11518,"3":0.0,"3.1":0.0,"3.2":0.0,"3.3":0.0,"3.4":0.0,"3.5":0.0,"3.6":0.0,"3.7":0.0,"3.8":0.0,"3.9":0.2071,"4":0.0,"4.1":0.0,"4.2":0.0,"4.3":0.0,"4.4":0.0,"4.5":0.0,"4.6":0.0,"4.7":0.0,"4.8":0.0,"4.9":0.0,"5":0.24563,"5.1":0.0,"5.2":0.0,"5.3":0.0,"5.4":0.0,"5.5":0.0,"5.6":0.0,"5.7":0.0,"5.8":0.0,"5.9":0.0,"5.99999999999999":0.20586,"6.09999999999999":0.0,"6.19999999999999":0.0,"6.29999999999999":0.0,"6.39999999999999":0.0,"6.49999999999999":0.0,"6.59999999999999":0.0,"6.69999999999999":0.0,"6.79999999999999":0.0,"6.89999999999999":0.0,"6.99999999999999":0.11758,"7.09999999999999":0.0,"7.19999999999999":0.0,"7.29999999999999":0.0,"7.39999999999999":0.0,"7.49999999999999":0.0,"7.59999999999999":0.0,"7.69999999999999":0.0,"7.79999999999999":0.0,"7.89999999999999":0.0,"7.99999999999999":0.04329,"8.09999999999999":0.0,"8.19999999999999":0.0,"8.29999999999999":0.0,"8.39999999999999":0.0,"8.49999999999999":0.0,"8.59999999999999":0.0,"8.69999999999999":0.0,"8.79999999999998":0.0,"8.89999999999998":0.0,"8.99999999999998":0.00976,"9.09999999999998":0.0,"9.19999999999998":0.0,"9.29999999999998":0.0,"9.39999999999998":0.0,"9.49999999999998":0.0,"9.59999999999998":0.0,"9.69999999999998":0.0,"9.79999999999998":0.0,"9.89999999999998":0.0008,"9.99999999999998":0.0008};
  public experimentResults: Array<any> = [];
  private experementsUrl = 'api/experement';
  private restoreUrl = 'api/restore';
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  addExperement(experement: Experement): Observable<Experement> {
    return this.http.post<Experement>(this.experementsUrl, experement, this.httpOptions).pipe(
      tap((res) => console.log(res)),
      tap((res) => this.experimentResults.push(res)),
      catchError(this.handleError<Experement>('addHero'))
    );
  }

  public resert() {
    this.experimentResults = [{data: {}, label: ''}];
  }

  /**
   * sHandle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
