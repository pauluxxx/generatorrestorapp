﻿using System.Linq;
using DAL.Entities;

namespace DAL.DBSeeds
{
    public static class DbInitializeer
    {
        public static void Initialize(DidenkoDB context)
        {
            context.Database.EnsureCreated();

            if (context.Generators.Any())
            {
                return; // DB has been seeded
            }
            var generators = new Generator[]
            {
                new Generator
                {
                    Title = "Binimial",
                    ClassRandomize="BLL.RandomizeClasses.BinomialRandGen",
                    GeneratorParameters= new GeneratorParameter[]{
                        new GeneratorParameter(){Parameter= new Parameter() {Key = "n", Value = 33,Min=1,Max=40,Step=1} },
                        new GeneratorParameter(){Parameter= new Parameter() {Key = "p", Value = 0.5f,Min=0,Max=1,Step=0.01f}}
                    }
                },
                 new Generator
                {
                    Title = "Laplasa",
                    ClassRandomize="BLL.RandomizeClasses.LaplasaRandGen",
                    GeneratorParameters= new GeneratorParameter[]{
                        new GeneratorParameter(){Parameter= new Parameter()  {Key = "m", Value = 0,Min=-5,Max=5,Step=0.1f} },
                        new GeneratorParameter(){Parameter= new Parameter(){Key = "b", Value = 1,Min=0,Max=5,Step=0.1f}}
                    }
                },
            };
            foreach (Generator g in generators)
            {
                context.Generators.Add(g);
            }
            var options = new Option[]
            {
                    new Option
                {
                    Name = "startap",
                    OptionParameter = new OptionParameter[]{
                        new OptionParameter(){Parameter= new Parameter() {Key = "NumberOfDots", Value = 100000,Min=10000,Max=10000000,Step=10000}},
                        new OptionParameter(){Parameter= new Parameter() {Key = "DensityPlot", Value = 100,Min=1,Max=200,Step=1}}
                    }
                },
            };
            foreach (Option g in options)
            {
                context.Options.Add(g);
            }
            context.SaveChanges();
        }
    }
}