import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-generator',
  templateUrl: 'generator.component.html',
})


export class GeneratorComponent {
  @Input() generator: Generator;
  hided = false;
  text_ok = 'Ok';
  @Output() onChanged = new EventEmitter<boolean>();

  ok() {
    this.onChanged.emit(false);
  }
}
