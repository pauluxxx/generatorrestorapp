﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class GeneratorRepository : IRepository<Generator>
    {

        private IDidenkoDb _db;
        public GeneratorRepository(IDidenkoDb _db)
        {
            this._db = _db;
        }
        public IDidenkoDb _dbContext
        {
            get { return this._db; }
        }

        public IEnumerable<Generator> Items
        {
            get
            {
               return _dbContext.Generators.Include(p=>p.GeneratorParameters).ThenInclude(e=>e.Parameter);
            }
        }
        public void Create(Generator item)
        => _dbContext.Generators.Add(item);
        public void Delete(Generator item)
        => _dbContext.Generators.Remove(item);

        public Generator Get(int id)
        {
            throw new System.NotImplementedException();
        }
        public  IEnumerable<Generator> GetAll()
        =>  _dbContext.Generators;


        public  void Update(Generator item)
        {
            _dbContext.Generators.Update(item);
             _dbContext.saveChanges();
        }
    }
}
