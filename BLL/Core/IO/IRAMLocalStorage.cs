﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Core.IO
{
    public interface IRAMLocalStorage
    {
        void LoadResultToStorage(int id, IDictionary<double, double> responceNumbers);
        IDictionary<double, double> getExperementResults(int id);
    }
}
