﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using MySQL.Data.EntityFrameworkCore.Infraestructure.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DidenkoDB : DbContext, IDidenkoDb
    {
        public DidenkoDB(DbContextOptions<DidenkoDB> options) : base(options)
        {
          
        }

        public DbSet<Generator> Generators { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Experement> Experements { get; set; }
        public DbSet<Option> Options { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GeneratorParameter>()
                .HasOne(pt => pt.Generator)
                .WithMany(p => p.GeneratorParameters);

            modelBuilder.Entity<GeneratorParameter>()
                .HasOne(pt => pt.Parameter)
                .WithMany(t => t.GeneratorParameters);

            modelBuilder.Entity<OptionParameter>()
                .HasOne(pt => pt.Option)
                .WithMany(p => p.OptionParameter);

            modelBuilder.Entity<OptionParameter>()
                .HasOne(pt => pt.Parameter)
                .WithMany(t => t.OptionParameter);
        }
        public void saveChanges()
        {
            this.SaveChanges();
        }
    }

    public interface IDidenkoDb
    {
        DbSet<Generator> Generators { get; }
        DbSet<Experement> Experements { get; }
        DbSet<Option> Options { get; }

        void saveChanges();
    }

    public static class GeneratorContextFactory
    {
        public static IDidenkoDb Create(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DidenkoDB>();
            optionsBuilder.UseMySQL(connectionString);
            var context = new DidenkoDB(optionsBuilder.Options);
            context.Database.EnsureCreated();
            return context;
        }
    }
}