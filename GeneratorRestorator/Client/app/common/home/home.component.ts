import {Component, OnInit} from '@angular/core';
import {GeneratorSevice} from '../shared/services/generator-sevice';
import {Option} from '../shared/models/option';
import {OptionService} from '../shared/services/option-service';
import {Experement} from '../shared/models/experement';
import {Generator} from '../shared/models/generator';
import {ExperimentService} from '../shared/services/experement-service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'app-home.component.html'
})
export class HomeComponent implements OnInit {

  text_header = 'Generate a new sequence';
  text_dropdown = 'Choose your distribution';
  text_generate = 'Generate';
  text_options = 'You may change options';
  text_opt_btn = 'Options';
  public generators: Generator[];
  public option: Option;
  public selectedGenerator: Generator;
  public isGenChosen = false;
  status: { isopen: boolean } = {isopen: false};

  constructor(private generatorsService: GeneratorSevice,
              private router: Router,
              private optionService: OptionService,
              private experimentService: ExperimentService) {
  }


  toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  change(value: boolean): void {
    this.status.isopen = value;
  }

  serveExperiment(): void {
    if (this.selectedGenerator && this.option) {
      let curentExperement: Experement = new Experement;
      curentExperement.option = this.option;
      curentExperement.generator = this.selectedGenerator;
      console.log(curentExperement);
      this.experimentService.addExperement(curentExperement).subscribe(exp => {
        console.log('all right');
        this.router.navigate(['/chart']);
      });
    }
  }

  onOkBtn(isOpen) {
    this.change(isOpen);
  }

  ngOnInit(): void {
    this.getGenerators();
    this.getOption();
  }

  private getGenerators(): void {
    this.generatorsService.getGenerators().then(generators => this.generators = generators);
  }

  private getOption(): void {
    this.optionService.getOptions('startap').then(option => {
      this.option = option;
    });
  }
}
