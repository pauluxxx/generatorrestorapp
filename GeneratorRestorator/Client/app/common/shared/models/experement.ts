import {Generator} from './generator';
import {Option} from './option';

export class Experement {
  id: number;
  generator: Generator;
  option: Option;
}
