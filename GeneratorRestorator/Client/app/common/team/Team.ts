export const teamMembers: TeamMember[] = [
  {
    name: 'Pavel Poymanov',
    position: 'Fullstack developer',
    resp: 'Implementing features, deploying'
  },
  {
    name: 'Aleksandra Bukreeva',
    position: 'Project manager',
    resp: 'Communication with tutor, tasks assigning'
  },
  {
    name: 'Roman Rogozhkin',
    position: 'BA/QA',
    resp: 'Requirements clarification, writing test cases'
  },
  {
    name: 'Maria Malyschenko',
    position: 'QA',
    resp: 'Writing test cases, test executing'
  },
  {
    name: 'Denys Sych',
    position: 'FE Developer',
    resp: 'Client side implementation, data visualisation'
  },
  {
    name: 'Stanyslav Dovgusha',
    position: 'BA',
    resp: 'Requirements clarification'
  }
];

interface TeamMember {
  name: string,
  position: string,
  resp: string
}
