import {Injectable} from '@angular/core';
import {GENERATORS} from '../../mocks/mock-generators';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Parameter} from '../models/parameter';
import {Generator} from '../models/generator';

@Injectable()
export class GeneratorSevice {
  private generatorsUrl = 'api/generator';

  constructor(private http: Http) {
  }

  getGenerators(): Promise<any []> {
    return this.http.get(this.generatorsUrl).toPromise().then(
      response => {
        let generators = [];
        response.json().map(function (e, i) {
          var params: Parameter[] = [];
          e.parameters.map(function (el) {
            params.push({key: el.key,
              value: el.value,
              min: el.min,
              max: el.max,
              step: el.step});
          });
          let generator:Generator =   {Id: e.id, title: e.title, paramters: params};
          generators.push(generator);
        });
        return generators;
      }
    ).catch(this.handleError);
  }


  getGeneratorsMock(): Promise<any []> {
    return Promise.resolve(GENERATORS);
  }

  private handleError(err: any): Promise<any> {
    console.error('Error with retrieving data from generators api', err);
    return Promise.reject(err.message || err);
  }
}
