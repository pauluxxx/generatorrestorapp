﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using GeneratorRestorator.Server.ClientEntities;
using System.Linq;
using System.Collections;

namespace DAL.Entities
{
    public class Generator :IEntity    {

        public Generator() { }

        public Generator(ClientGenerator generator)
        {
            this.Id = generator.Id;
            this.Title = generator.title;
          //  this.ClassRandomize = "";//todo change this
            this.GeneratorParameters = generator.paramters.Select(e=>new GeneratorParameter(this,e)).ToList();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        [Required]
        public IEnumerable<GeneratorParameter> GeneratorParameters { get; set; }
        public string ClassRandomize { get;set;}
    }
}
