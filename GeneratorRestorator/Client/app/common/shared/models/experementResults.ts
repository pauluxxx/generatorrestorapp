export class ExperementResults {
  Mean: number;
  Varience: number;
  Min: number;
  Max: number;
  Data: any;
  Name: string;
}
