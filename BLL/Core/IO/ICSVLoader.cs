﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;

namespace BLL.Core.IO
{
    public interface ICSVLoader
    {
        void Write(Experement experement, List<double> tempList);
    }
}
