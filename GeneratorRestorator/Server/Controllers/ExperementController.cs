﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAL.Entities;
using GeneratorRestorator.Server.ClientEntities;
using BLL.Managers.Abstact;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GeneratorRestorator.Server.Controllers
{
  [Route("api/[controller]")]
  public class ExperementController : Controller
  {

    private IExperementManager manager;
    public ExperementController(IExperementManager manager)
    {
      this.manager = manager;
    }
    // GET: api/values
    [HttpGet]
    public IEnumerable<string> Get()
    {
      manager.startExperement(new Experement());
      return new string[] { "value1", "value2" };
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/experement
    [HttpPost]
    public ResultsGenerating Post([FromBody]ClientExperement value)
    {
      var StartedExperement = new Experement(value);
      var res = manager.startExperement(StartedExperement);
      return res;
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }

}
