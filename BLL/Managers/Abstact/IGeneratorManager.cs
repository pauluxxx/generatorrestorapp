﻿using DAL.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Managers.Abstact
{
    public interface IGeneratorManager
    {
        IEnumerable<Generator> Generators();
        IEnumerable GeneratorsWithParameters();
        Generator getGenerator(int id);
    }
}
