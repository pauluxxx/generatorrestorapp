
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './common/home/home.component';
import {NgModule} from '@angular/core';
import {TeamComponent} from "./common/team/team.component";
import {AboutComponent} from "./common/about/about.component";
import {ChartComponent} from "./common/chart/chart.component";

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'team', component: TeamComponent},
  {path: 'about', component: AboutComponent},
  {path: 'chart', component: ChartComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
