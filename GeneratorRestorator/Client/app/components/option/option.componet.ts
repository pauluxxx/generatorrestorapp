import {Option} from '../../common/shared/models/option';
import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-option',
  templateUrl: 'option.component.html'
})

export class OptionComponet {
  text_title: string = 'Current options';
  text_ok: string = 'Ok';
  @Input() option: Option;
}
