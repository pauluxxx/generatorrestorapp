import {Component} from '@angular/core';
import {teamMembers} from "./Team";


@Component({
  selector: 'app-team',
  templateUrl: 'team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent {
  public teamMembers = teamMembers;
}
