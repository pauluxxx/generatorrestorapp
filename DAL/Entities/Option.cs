﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GeneratorRestorator.Server.ClientEntities;
using System.Linq;

namespace DAL.Entities
{
    public class Option : IEntity
    {
        public Option() { }
        public Option(ClientOption option)
        {
            this.Id = option.id;
            this.Name = option.name;
            this.OptionParameter = option.paramters.Select(e=>new OptionParameter(e,this)).ToList();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public IEnumerable<OptionParameter> OptionParameter { get; set; }

    }
}