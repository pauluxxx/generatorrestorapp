﻿using DAL.Entities;
using System.Collections.Generic;

namespace BLL.Core
{
    public interface IExperementExequtor
    {
        Experement Experement { get; set; }
        IDictionary<double, double> ServeExperement();
        double getMean();
        double getVarience();
    }
}