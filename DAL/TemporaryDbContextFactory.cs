﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class TemporaryDbContextFactory : IDbContextFactory<DidenkoDB>
    {
        public DidenkoDB Create(DbContextFactoryOptions options)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DidenkoDB>();
            var connectionString = "server=localhost;userid=pavel;pwd=12345;port=3306;database=didenkoDB;sslmode=none";
            optionsBuilder.UseMySQL(connectionString);
            //Ensure database creation
            var context = new DidenkoDB(optionsBuilder.Options);
            context.Database.EnsureCreated();

            return context;
        }
    }
}
