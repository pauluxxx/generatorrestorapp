﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class GeneratorParameter:IEntity
    {
        public GeneratorParameter() { 
}
        public GeneratorParameter(Generator generator, Parameter e)
        {
            this.Generator = generator;
            this.Parameter = e;
        }

        public int Id { get; set; }
        public Generator Generator { get; set; }
        public Parameter Parameter{ get; set; }
    }
}
