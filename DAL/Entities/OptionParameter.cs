﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
   public class OptionParameter:IEntity
    {
        public OptionParameter() { }
        public OptionParameter(Parameter e,Option o)
        {
            this.Option = o;
            this.Parameter = e;
        }

        public int Id { get; set; }
        public Option Option { get; set; }
        public Parameter Parameter { get; set; }
    }
}
