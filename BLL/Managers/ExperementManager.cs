﻿using BLL.Managers.Abstact;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using BLL.Core;
using DAL.Repositories;
using System.Linq;
using BLL.Core.IO;
using GeneratorRestorator.Server.ClientEntities;
namespace BLL.Managers
{
    public class ExperementManager : IExperementManager
    {

        private IRepository<Experement> repository;
        private IExperementExequtor exe;
        private IGeneratorManager managerG;
        private IOptionManager managerO;
        private IRAMLocalStorage LocalStorageGenerations { get; set; }
        public ExperementManager(IExperementExequtor exe, IRepository<Experement> repository, IGeneratorManager managerG, IOptionManager managerO, IRAMLocalStorage ramLS)
        {
            this.exe = exe;
            this.repository = repository;
            this.managerG = managerG;
            this.managerO = managerO;
            this.LocalStorageGenerations = ramLS;
        }
        public ResultsGenerating startExperement(Experement curentExperement)
        {
            exe.Experement = curentExperement;
            exe.Experement.Generator.ClassRandomize = managerG.Generators().First(e => e.Id == curentExperement.Generator.Id).ClassRandomize;
            var results = exe.ServeExperement();
            curentExperement.Generator = managerG.Generators().First(e => e.Id == curentExperement.Generator.Id);//fixx
            curentExperement.Option = managerO.getOptions().First(e => e.Id == curentExperement.Option.Id);//fixx
            DateTime dateValue = DateTime.Now;
            string MySQLFormatDate = dateValue.ToString("yyyy-MM-dd HH:mm:ss");
            curentExperement.TimeOfGeneration = MySQLFormatDate;
            repository.Create(curentExperement);
            LocalStorageGenerations.LoadResultToStorage(curentExperement.Id, results);
            return new ResultsGenerating { Data = LocalStorageGenerations.getExperementResults(curentExperement.Id), Max = (results.Keys.Count != 0) ? results.Keys.Max() : 0, Min = (results.Keys.Count != 0) ? results.Keys.Min() : 0, Mean = exe.getMean(), Varience = exe.getVarience(), Name = curentExperement.Generator.Title + " " + MySQLFormatDate };

        }

        public IDictionary<double, double> getRestoredInterpolation(DataClass data)
        {
            double[] x = new double[data.data.Keys.Count];
            var i = 0;
            foreach(double d in data.data.Keys)
            {
                x[i++] = d;
            }
            double[] y = new double[data.data.Values.Count];
             i = 0;
            foreach (double d in data.data.Values)
            {
                y[i++] = d;
            }
            IDictionary<double, double> res = new Dictionary<double, double>();
            alglib.barycentricinterpolant p;
            alglib.polynomialbuild(x, y, out p);
            foreach(double d in x)
            {
                res.Add(d,alglib.barycentriccalc(p, d));
            }
            return res;
        }

        public IDictionary<double, double> getRestoredMNK(DataClass data)
        {
            double[] x = new double[data.data.Keys.Count];
            var i = 0;
            foreach (double d in data.data.Keys)
            {
                x[i++] = d;
            }
            double[] y = new double[data.data.Values.Count];
            i = 0;
            foreach (double d in data.data.Values)
            {
                y[i++] = d;
            }
            int info;
            double v;
            alglib.spline1dinterpolant s;
            alglib.spline1dfitreport rep;
            double rho;
            rho = -1.0;
            alglib.spline1dfitpenalized(x, y, 10, rho, out info, out s, out rep);

            IDictionary<double, double> res = new Dictionary<double, double>();
            foreach (double d in x)
            {
                res.Add(d, alglib.spline1dcalc(s, d));
            }
            return res;
        }
    }
}
