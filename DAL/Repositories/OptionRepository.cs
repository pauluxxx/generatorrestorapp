﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class OptionRepository : IRepository<Option>
    {

        private IDidenkoDb _db;
        public OptionRepository(IDidenkoDb _db)
        {
            this._db = _db;
        }
        public IDidenkoDb _dbContext
        {
            get { return this._db; }
        }
        public IEnumerable<Option> Items => _dbContext.Options.Include(p => p.OptionParameter).ThenInclude(e => e.Parameter);

        public void Create(Option item)
        {
            throw new NotImplementedException();
        }

        public void Delete(Option item)
        {
            throw new NotImplementedException();
        }

        public Option Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Option> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update(Option item)
        {
            throw new NotImplementedException();
        }
    }
}
