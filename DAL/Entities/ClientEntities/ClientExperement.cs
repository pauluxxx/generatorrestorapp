﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneratorRestorator.Server.ClientEntities
{

  public class ClientExperement
  {
    public ClientGenerator Generator { get; set; }
    public ClientOption Option { get; set; }

   
  }

  public class ClientGenerator
  {
    public int Id { get; set; }
    public string title { get; set; }
    public IEnumerable<Parameter> paramters { get; set; }

   
  }
  public class ClientOption
  {
    public int id { get; set; }
    public string name { get; set; }
    public IEnumerable<Parameter> paramters { get; set; }

    
  }
    public class ResultsGenerating
    {
        public double Mean{ get; set; }
        public double Varience{ get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public IDictionary<double, double> Data { get; set; }
        public string Name { get; set; }
        public ResultsGenerating()
        {

        }

    }
    public class DataClass
    {
        public IDictionary<double, double> data { get; set; }
    }
}
