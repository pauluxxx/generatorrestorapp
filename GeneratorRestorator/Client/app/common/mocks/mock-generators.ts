import {Generator} from '../shared/models/generator';

export const GENERATORS: Generator [] = [
  {
    Id: 1,
    title: 'Binomial',
    paramters: [{key: 'a', value: 1, min: 0, max: 10, step: 1}, {key: 'b', value: 2, min: 1, max: 5, step: 0.1}]
  },
  {
    Id: 2,
    title: 'Laplasa',
    paramters: [{key: 'vsa', value: 1, min: 0, max: 10, step: 1}, {key: 'b', value: 2, min: 1, max: 5, step: 0.1}]
  },]
;


