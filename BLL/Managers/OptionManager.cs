﻿using BLL.Managers.Abstact;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using System.Collections;
using DAL.Repositories;
using System.Linq;

namespace BLL.Managers
{
    public class OptionManager : IOptionManager
    {
        IRepository<Option> repository;
        public OptionManager(IRepository<Option> repository)
        {
            this.repository = repository;
        }
        public Option getOption(string name)
        {
            return repository.Items.Single(e=>e.Name==name);
        }

        public Option getOption(int id)
        {
            return repository.Items.FirstOrDefault(e => e.Id == id);

        }

        public IEnumerable<Option> getOptions()
        {
            return repository.Items;
        }
    }
}
