﻿using BLL.Core.IO;
using BLL.RandomizeClasses;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Core
{

    public class ExperementExequtor : IExperementExequtor
    {
        public Experement Experement { get; set; }
        private IDictionary<double, double> responceNumbers;
        public int columns { get; set; }
        public int numberOfSteps { get; set; }
        private ICSVLoader CSVLoder { get; set; }
        private IRAMLocalStorage LocalStorageGenerations { get; set; }
        private IRandGenerator classOfRandGen;
        public ExperementExequtor() { }
        public ExperementExequtor(ICSVLoader CSVLoder, IRAMLocalStorage LocalStorageGenerations)
        {
            this.CSVLoder = CSVLoder;
            this.LocalStorageGenerations = LocalStorageGenerations;

        }
        public IDictionary<double, double> ServeExperement()
        {
            intitializeParams();
            classOfRandGen = createRandGen(this.Experement.Generator.ClassRandomize);
            List<double> tempList = new List<double>();
            for (int i = 0; i < numberOfSteps; i++)
            {
                double randomedNumber = classOfRandGen.nextDouble();
                tempList.Add(randomedNumber);

            }
            responceNumbers = transferInfoInTable(tempList);
            //storeInDBWhenComplite
            return responceNumbers;
        }


        private IDictionary<double, double> transferInfoInTable(List<double> tempList)
        {
            var max = tempList.Max();
            var min = tempList.Min();
            var interval = (max - min) / columns;
            double[] den = new double[columns];

            for (int i = 0; i < columns; i++)
            {
                den[i] = 0;
            }

            int colums = columns;
            double[,] intervals = new double[colums, 2];
            double widthInterval = (max - min) / colums;
            double mathsum = 0.0;
            double mathsumSqr = 0.0;
            for (int i = 0; i < colums; i++)
            {
                den[i] = 0;
            }
            //int i=0,j=0;
            var start = min;
            var count = 0;
            while (start < max)
            {
                intervals[count, 0] = start;
                intervals[count, 1] = start + widthInterval;
                start += widthInterval;
                if (count < colums - 1) { count++; }
            }
            tempList.ForEach(e =>
            {
                mathsum += e;//mathoghid
                mathsumSqr += e * e;
                for (int j = 0; j < colums; j++)
                {
                    if (e > intervals[j, 0] && e <= intervals[j, 1])
                    {
                        den[j]++;
                        break;
                    }
                }

            });
            var responceNumbersT = new Dictionary<double, double>();
             start = min;
             count = 0;
            while (start < max)
            {
                responceNumbersT.Add(start, den[count]/ numberOfSteps);
                start += widthInterval;
                if (count < colums - 1) { count++; }
            }
            return responceNumbersT;

        }

        private void intitializeParams()
        {
            numberOfSteps = (int)this.Experement.Option.OptionParameter.FirstOrDefault(e => e.Parameter.Key == "NumberOfDots").Parameter.Value;
            columns = (int)this.Experement.Option.OptionParameter.FirstOrDefault(e => e.Parameter.Key == "DensityPlot").Parameter.Value;
        }

        private IRandGenerator createRandGen(string classRandomize)
        {

            var an = new AssemblyName("BLL");
            Assembly asm = Assembly.Load(an);
            Type t = asm.GetType(classRandomize, true, true);
            object obj = Activator.CreateInstance(t);
            MethodInfo method = t.GetMethod("TuneGenertor");
            object result = method.Invoke(obj, new object[] { Experement.Generator });
            return (IRandGenerator)obj;
        }

        public double getMean()
        {
            return classOfRandGen.getMean();
        }

        public double getVarience()
        {
            return classOfRandGen.getVariance();
        }
    }
}
