﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    class GeneratorParameters
    {
        public int GeneratorId { get; set; }
        public Generator Generator { get; set; }
        public int ParameterId { get; set; }
        public Parameter Parameter{ get; set; }
    }
}
